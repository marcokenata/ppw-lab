from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.urls import reverse
from django.http import HttpRequest
import lab_10.csui_helper as csui_helper
import lab_10.custom_auth as custom_auth
from .views import *
from django.test import TestCase
from django.test import Client
import requests
import environ
from .omdb_api import get_detail_movie, create_json_from_dict, search_movie

root = environ.Path('__file__')
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env()
# Create your tests here.
class Lab10UnitTest(TestCase):
    def test_lab_10_url_is_exist(self):
        response = Client().get('/lab-10/')
        self.assertEqual(response.status_code, 200)

    def test_lab_10_page_when_user_is_logged_in_or_not(self):
        # not logged in, render login template
        response = self.client.get('/lab-10/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('lab_10/login.html')

        # logged in, redirect to profile page
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-10/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_10/dashboard.html')

    def test_direct_access_to_dashboard_url(self):
        # not logged in, redirect to login page
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 302)

        # logged in, render profile template
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = self.client.post('/lab-10/custom_auth/login/', {'username': username, 'password': password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)

    """
    UnitTest for csui_helper.py
    """
    def test_get_access_token(self):
        username_to_input="hehehe"
        password_to_input="123456"
        with self.assertRaises(Exception) as context:
            csui_helper.get_access_token(username_to_input, password_to_input)
        self.assertIn(username_to_input, str(context.exception))

    def test_get_client_id(self):
        self.assertEqual(csui_helper.get_client_id(), 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')

    def test_verify_user(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        access_token = csui_helper.get_access_token(username, password)
        verify_user = csui_helper.verify_user(access_token)
        self.assertTrue(verify_user is not None)

    def test_get_data_user(self):
        access_token = csui_helper.get_access_token(env("SSO_USERNAME"), env("SSO_PASSWORD"))
        verify_user = csui_helper.verify_user(access_token)
        data_user = csui_helper.get_data_user(access_token, verify_user["identity_number"])

    """
    UnitTest for custom_auth.py
    """
    def test_for_login_and_logout(self):
        username = env("SSO_USERNAME")
        password = env("SSO_PASSWORD")
        response = Client().post('/lab-10/custom_auth/login/', {'username': username, 'password':password})
        self.assertEqual(response.status_code, 302)
        response = Client().get('/lab-10/custom_auth/logout/')
        self.assertEqual(response.status_code, 302)


    """
    Another UnitTest
    """

    def test_add_watch_later_and_list_watch_later(self):
        # test jika id yang ditambahkan tidak valid (saat penambahan secara manual)
        response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tidakada'}))
        self.assertEqual(response_post.status_code, 302)
        # test jika menambahkan dengan login (data disimpan di database)
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-10:auth_login'),
                                         {'username': self.username, 'password': self.password})
        response_post = self.client.get(reverse('lab-10:dashboard'))
        response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt1396484'}))
        self.assertEqual(response_post.status_code, 302)
        response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
        html_response = response_post.content.decode('utf8')
        self.assertIn('Berhasil tambah movie ke Watch Later', html_response)
        # test list_watch_later dengan login (data diambil dari database)
        response_post = self.client.get(reverse('lab-10:list_watch_later'))
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, "lab_10/movie/watch_later.html")
        html_response = response_post.content.decode('utf8')
        self.assertIn('It', html_response)
        # test add_watched_movie but not on database
        response_post = self.client.get(reverse('lab-10:add_watched_movie', kwargs={'id': 'tt3300542'}))
        self.assertEqual(response_post.status_code, 302)
        response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt3300542'}))
        html_response = response_post.content.decode('utf8')
        self.assertIn('Movie is not on DATABASE!', html_response)
        # test add_watched_movie
        response_post = self.client.get(reverse('lab-10:add_watched_movie', kwargs={'id': 'tt1396484'}))
        self.assertEqual(response_post.status_code, 302)
        response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
        html_response = response_post.content.decode('utf8')
        # test list_watched_movie dengan login (data diambil dari database)
        response_post = self.client.get(reverse('lab-10:list_watched_movie'))
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, "lab_10/movie/watched_movie.html")
        html_response = response_post.content.decode('utf8')
        self.assertIn('It', html_response)
        # test jika id yang sama ditambahkan kembali secara manual dengan login
        response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt1396484'}))
        self.assertEqual(response_post.status_code, 302)
        response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
        html_response = response_post.content.decode('utf8')
        self.assertIn('Movie already exist on DATABASE! Hacking detected!', html_response)
        # menambahkan satu movie lagi dengan login
        response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt3874544'}))
        self.assertEqual(response_post.status_code, 302)
        response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt3874544'}))
        html_response = response_post.content.decode('utf8')
        self.assertIn('Berhasil tambah movie ke Watch Later', html_response)
        # logout
        response_post = self.client.post(reverse('lab-10:auth_logout'))
        # test jika menambahkan tanpa login (data akan disimpan di session)
        response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt1396484'}))
        self.assertEqual(response_post.status_code, 302)
        response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
        html_response = response_post.content.decode('utf8')
        self.assertIn('Berhasil tambah movie ke Watch Later', html_response)
        # test list_watch_later tanpa login (data diambil dari session)
        response_post = self.client.get(reverse('lab-10:list_watch_later'))
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, "lab_10/movie/watch_later.html")
        html_response = response_post.content.decode('utf8')
        self.assertIn('It', html_response)
        # test jika id yang sama ditambahkan kembali secara manual tanpa login
        response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt1396484'}))
        self.assertEqual(response_post.status_code, 302)
        response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
        html_response = response_post.content.decode('utf8')
        self.assertIn('Movie already exist on SESSION! Hacking detected!', html_response)
        # menambahkan satu movie lagi tanpa login
        response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt4649466'}))
        self.assertEqual(response_post.status_code, 302)
        response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt4649466'}))
        html_response = response_post.content.decode('utf8')
        self.assertIn('Berhasil tambah movie ke Watch Later', html_response)
        # jika sudah menambahkan namun belum login, maka setelah login movie dari session yang belum ada di database
        # akan disimpan di dalam database
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-10:auth_login'),
                                         {'username': self.username, 'password': self.password})
        response_post = self.client.get(reverse('lab-10:dashboard'))
        # logout
        response_post = self.client.post(reverse('lab-10:auth_logout'))

    def test_list_movie_page_exist(self):
        # login
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-10:auth_login'),
                                         {'username': self.username, 'password': self.password})
        response_post = self.client.get(reverse('lab-10:movie_list'))
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, "lab_10/movie/list.html")
        response_post = self.client.get(reverse('lab-10:movie_list'), {'judul': 'It', 'tahun': '2017'})
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, "lab_10/movie/list.html")

        # logout
        response_post = self.client.post(reverse('lab-10:auth_logout'))

    def test_detail_page(self):
        # test jika tidak login (tidak ada key 'user_login' di session)
        response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, "lab_10/movie/detail.html")
        # login
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-10:auth_login'),
                                         {'username': self.username, 'password': self.password})
        response_post = self.client.get(reverse('lab-10:dashboard'))
        response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, "lab_10/movie/detail.html")
        # logout
        response_post = self.client.post(reverse('lab-10:auth_logout'))

    def test_api_search_movie(self):
        # init search
        response = Client().get('/lab-10/api/movie/-/-/')
        self.assertEqual(response.status_code, 200)
        # search movie by title
        response = Client().get('/lab-10/api/movie/justice/-/')
        self.assertEqual(response.status_code, 200)
        # search movie by title and year
        response = Client().get('/lab-10/api/movie/justice/2016/')
        self.assertEqual(response.status_code, 200)
        # 0 > number of result <= 3
        response = Client().get('/lab-10/api/movie/Guardians of Galaxy/2016/')
        self.assertEqual(response.status_code, 200)
        # not found
        response = Client().get('/lab-10/api/movie/zabolaza/-/')
        self.assertEqual(response.status_code, 200)
