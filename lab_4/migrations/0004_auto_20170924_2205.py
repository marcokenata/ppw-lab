# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-24 15:05
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0003_auto_20170924_2203'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=datetime.date.today, verbose_name='Date'),
        ),
    ]
