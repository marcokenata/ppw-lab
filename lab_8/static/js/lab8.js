// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '304211886745930',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        render(true);
      }
      else if (response.status === 'not_authorized') {
        render(false);
      }
      else {
        render(false);
      }
  });

  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri





  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="profile">' +
                      '<button class="logout" onclick="facebookLogout()">Logout</button>'+
          '  <div class="jumbotron" style="background:url('+user.cover.source+');height: 100%;width: 100%;position: relative;background-size:cover;">'+
            '  <div class = "container">'+
              '  <img class="picture" src="' + user.picture.data.url + '" alt="profpic" style="opacity:1;margin-top:10%;margin-left:40%;"/>' +
              '  <h1><font style="font-size:32px;padding:20px;color:white;margin-top:23%;margin-left:35%;">Hello! '+ user.name + '</font></h1>' +
            '  </div>' +
          '  </div>' +
            '<div class="container-fluid" style="padding:10px;font-family: \'Philosopher\', sans-serif;">' +
            '<div class="col-sm-3" style="width:300px;">'+
              '<span class="glyphicon glyphicon-envelope" aria-hidden="true" style="font-size:24px;margin-left:45%;padding:5px;"></span>'+
              '<h3 style="padding:10px;">' + user.email + '</h3>' +
              '</div>' + 
              '<div class="col-sm-3" style="width:500px;">'+
              '<span class="glyphicon glyphicon-align-justify" aria-hidden="true" style="font-size:24px;margin-left:45%;padding:5px;"></span>'+
              '<h3 style="padding:10px;">' + user.about + '</h3>' +
              '</div>' + 
              '<div class="col-sm-3" style="width:300px;">'+
               '<span class="glyphicon glyphicon-user" aria-hidden="true" style="font-size:24px;margin-left:45%;padding:5px;"></span>'+
               '<h3 style="padding:5px;text-align:center;">' + user.gender + '</h3>' +
                '</div>' + 
            '</div>' +
          '</div>' +
          '<div class="container" style="padding:10px;">'+
          '<h1>Post Facebook Status</h1>' +
          '<textarea style="padding:20px;resize:none;" id="postInput" cols="150" rows="5" class="post" placeholder="Ketik Status Anda" /><br>' +
          '<button class="postStatus" onclick="postStatus()" style="padding:10px;">Post ke Facebook</button>' +
          '</div>' 
        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="well" style="padding:10px;border:2px solid grey;">' +
                '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" width=100 height=100/>' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="well" style="padding:10px;border:2px solid grey;">' +
                '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" width=100 height=100/>' +
                  '<h1>' + value.message + '</h1>' +
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="well" style="padding:10px;border:2px solid grey;"> ' +
                '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" width=100 height=100/>' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            }
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
  };

  const facebookLogin = () => {
    FB.login(function(response){
     console.log(response);
     render(true);
     uid = response.authResponse && response.authResponse.userID || null;
  }, {scope:'public_profile,user_posts,publish_actions,user_about_me,email'})

    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
  };

  const facebookLogout = () => {
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.logout();
        render(false);
      }
  });

    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
        FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.api('/me?fields=id,name,about,email,gender,cover,picture.width(168).height(168)', 'GET', function(response){
          console.log(response);
          if (response && !response.error) {
            /* handle the result */
            fun(response);
          }
        });
      }
  });

  };

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.api('/me/posts', 'GET', function(response){
          console.log(response);
          if (response && !response.error) {
            /* handle the result */
            fun(response);
          }
        
        });
      }
  });

  };

  const postFeed = (message) => {
    FB.api('/me/feed', 'POST', {message:message});
    alert("Your message have been posted");
    $('#postInput').val('');
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };

