$(document).ready(function () {
    themes = [{"id": 0, "text": "Red", "bcgColor": "#F44336", "fontColor": "#FAFAFA"},
    {"id": 1, "text": "Pink", "bcgColor": "#E91E63", "fontColor": "#FAFAFA"},
    {"id": 2, "text": "Purple", "bcgColor": "#9C27B0", "fontColor": "#FAFAFA"},
    {"id": 3, "text": "Indigo", "bcgColor": "#3F51B5", "fontColor": "#FAFAFA"},
    {"id": 4, "text": "Blue", "bcgColor": "#2196F3", "fontColor": "#212121"},
    {"id": 5, "text": "Teal", "bcgColor": "#009688", "fontColor": "#212121"},
    {"id": 6, "text": "Lime", "bcgColor": "#CDDC39", "fontColor": "#212121"},
    {"id": 7, "text": "Yellow", "bcgColor": "#FFEB3B", "fontColor": "#212121"},
    {"id": 8, "text": "Amber", "bcgColor": "#FFC107", "fontColor": "#212121"},
    {"id": 9, "text": "Orange", "bcgColor": "#FF5722", "fontColor": "#212121"},
    {"id": 10, "text": "Brown", "bcgColor": "#795548", "fontColor": "#FAFAFA"}];

    //menambahkan json yang berisi array themes
    localStorage.setItem("themes", JSON.stringify(themes));

    //inisiasi variabel mySelect
    mySelect = $('.my-select').select2();

    //populate data pada mySelect
    mySelect.select2({
        'data': JSON.parse(localStorage.getItem("themes")
        )
    });

    var arraysOfTheme = JSON.parse(localStorage.getItem("themes"));
    var indigo = arraysOfTheme[3]; //themeawal
    var themeawal = indigo;
    var yangdipilih = themeawal;
    var perubahan = themeawal;

    if (localStorage.getItem("yangdipilih") !== null) {
        perubahan = JSON.parse(localStorage.getItem("yangdipilih"));
    }

    //pake yang perubahan
    yangdipilih = perubahan;

    $('body').css(
        {
            "background-color": yangdipilih.bcgColor,
            "font-color": yangdipilih.fontColor
        }
    );

    //Chat box

    var cls = "msg-receive";
    $('#teks').keypress(function (e) {
        if (e.which == 13) {
            var msg = $('#teks').val();
            var old = $('#tempat').html();
            if (msg.length === 0) {
                alert("Message kosong");
            }
            else {
                cls = (cls == "msg-send") ? "msg-receive" : "msg-send"
                $('#tempat').html(old + '<p class=' + cls + '>' + msg + '</p>')
                $('#teks').val("");

            }
        }
    });

    $('#tempat').keyup(function (e) {
        if (e.which == 13) {
            $('#teks').val("");
        }
    });
    //END

    $('.apply-button').on('click', function () {

        var option = mySelect.val();

        if (option < arraysOfTheme.length) {
            yangdipilih = arraysOfTheme[option];
        }

        $('body').css(
            {
                "background-color": yangdipilih.bcgColor,
                "font-color": yangdipilih.fontColor
            }
        );

        localStorage.setItem("yangdipilih", JSON.stringify(yangdipilih));
    })
});